// ==UserScript==
// @name         Filter out approved merge requests
// @version      4
// @match        https://gitlab.com/*/merge_requests*state=opened*
// @match        https://gitlab.com/*/merge_requests
// @updateURL    https://gitlab.com/jan.hnizdil/user-scripts/-/raw/master/gitlab/merge-requests/filter-out-approved.user.js
// @downloadURL  https://gitlab.com/jan.hnizdil/user-scripts/-/raw/master/gitlab/merge-requests/filter-out-approved.user.js
// ==/UserScript==

(function($) {
    'use strict';
	$('li.merge-request:has(li:contains(Approved))').remove()
})(jQuery);
