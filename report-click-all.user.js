// ==UserScript==
// @name         Report Click All
// @description  Select all days in report calendar
// @version      1
// @updateURL    https://gitlab.com/jan.hnizdil/user-scripts/-/raw/master/report-click-all.user.js
// @downloadURL  https://gitlab.com/jan.hnizdil/user-scripts/-/raw/master/report-click-all.user.js
// @match        https://report.livesport.eu/absences/*
// @icon         https://report.livesport.eu/res/img/favicon.ico
// @run-at       context-menu
// ==/UserScript==

(() => {
    'use strict'

    function dayNodes() {
        return document.querySelectorAll('#default-datepick td > a[href]:not(.datepick-set)')
    }

    const count = dayNodes().length

    for (var i = 0; i < count; i++) {
        setTimeout(
            ((i) => {
                dayNodes()[i].click()
                console.info(dayNodes()[i])
            })(i),
            i * 100
        )
    }
})()
